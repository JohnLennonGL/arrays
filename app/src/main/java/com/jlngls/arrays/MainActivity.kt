package com.jlngls.arrays

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var nomes = arrayOf("John","suh","pai","mae")
        nomes[3] = "moacir"
    }
}